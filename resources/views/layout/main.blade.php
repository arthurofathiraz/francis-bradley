@extends('../layout/base')

@section('body')
    <body class="main">
    {{-- untuk notifikasi --}}
    <div id="notification-header-success" class="toastify-content hidden flex">
        <i class="text-theme-9" data-feather="check-circle"></i>
        <div class="ml-4 mr-4">
            <div id="notification-message-success" class="font-medium"></div>
            <div id="notification-details-success" class="text-gray-600 mt-1"></div>
        </div>
    </div>

    <div id="notification-header-error" class="toastify-content hidden flex">
        <i class="text-theme-6" data-feather="x-circle"></i>
        <div class="ml-4 mr-4">
            <div id="notification-message-error" class="font-medium"></div>
            <div id="notification-details-error" class="text-gray-600 mt-1"></div>
        </div>
    </div>

    @yield('content')
    @include('../layout/components/dark-mode-switcher')

    <!-- BEGIN: JS Assets-->
    <script
        src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=&libraries=places"></script>
    <script src="{{ mix('dist/js/app.js') }}"></script>
    <!-- END: JS Assets-->

    @yield('script')
    </body>
@endsection
