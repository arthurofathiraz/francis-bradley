<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'fathiraz arthuro',
                'email' => 'arthurofathiraz@gmail.com',
                'username' => 'fathiraz',
                'password' => Hash::make('B1smillahFathiraz'),
                'role' => 'ADMIN',
            ],
            [
                'name' => 'nisrina nur aini',
                'email' => 'nisrinaini@gmail.com',
                'username' => 'nisrina',
                'password' => Hash::make('B1smillahNisrina'),
                'role' => 'ADMIN',
            ],
            [
                'name' => 'rizqi reza',
                'email' => 'kija@gmail.com',
                'username' => 'kija',
                'password' => Hash::make('B1smillahKija'),
                'role' => 'USER',
            ],
            [
                'name' => 'wedding organizer rostic',
                'email' => 'rostic@gmail.com',
                'username' => 'rostic',
                'password' => Hash::make('B1smillahRostic'),
                'role' => 'WO',
            ],
            [
                'name' => 'keluarga',
                'email' => 'keluarga@gmail.com',
                'username' => 'keluarga',
                'password' => Hash::make('B1smillahKeluarga'),
                'role' => 'USER',
            ],
            [
                'name' => 'nana',
                'email' => 'nanasuryana@gmail.com',
                'username' => 'nana',
                'password' => Hash::make('B1smillahNana'),
                'role' => 'USER',
            ],
        ]);
    }
}
