<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $male_name
 * @property string $male_nickname
 * @property string $male_photo
 * @property string $male_description
 * @property string $female_name
 * @property string $female_nickname
 * @property string $female_photo
 * @property string $female_description
 * @property string $akad_date
 * @property string $akad_name
 * @property string $akad_time
 * @property string $akad_longitude
 * @property string $akad_latitude
 * @property string $akad_maps
 * @property string $walimah_date
 * @property string $walimah_name
 * @property string $walimah_time
 * @property integer $walimah_longitude
 * @property integer $walimah_latitude
 * @property string $walimah_maps
 * @property string $guide_file
 * @property string $invitation_file
 * @property string $qris_file
 * @property string $protocol_file
 * @property string $song_file
 * @property string $bank_account_number
 * @property string $bank_account_name
 * @property string $bank_account
 * @property string $zoom_live_url
 * @property string $meet_live_url
 * @property string $instagram_live_id
 * @property string $created_at
 * @property string $updated_at
 */
class Configuration extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['male_name', 'male_nickname', 'male_photo', 'male_description', 'female_name', 'female_nickname', 'female_photo', 'female_description', 'akad_date', 'akad_name', 'akad_time', 'akad_longitude', 'akad_latitude', 'akad_maps', 'walimah_date', 'walimah_name', 'walimah_time', 'walimah_longitude', 'walimah_latitude', 'walimah_maps', 'guide_file', 'guide_old_file', 'invitation_file', 'qris_file', 'protocol_file', 'song_file', 'bank_account_number', 'bank_account_name', 'bank_account', 'zoom_live_url', 'meet_live_url', 'instagram_live_id', 'created_at', 'updated_at'];

}
