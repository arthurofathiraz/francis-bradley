<?php

namespace App\Http\Controllers;

use App\Models\Configuration;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function userView()
    {
        return view('pages/user/user');
    }

    public function userAddView(Request $request)
    {
        $configuration = Configuration::first();

        return view('pages/user/user-add', [
            'configuration' => $configuration
        ]);
    }

    public function userAdd(Request $request)
    {
        $rule = [
            'name' => 'required|min:1|max:100',
            'email' => 'required|email|unique:users,email',
            'username' => 'required|min:1|max:100',
            'password' => 'required|min:1|max:200',
            'role' => 'required|in:ADMIN,USER,WO',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // set data
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'role' => $request->role,
        ];

        if (isset($request->password) && !empty($request->password)) {
            $data['password'] = Hash::make($request->password);
        }

        // update data
        $update = User::create($data);
        if (!$update) {
            return response()->json(response_error(
                "error create data"
            ), 200);
        }

        $user = User::where('id', $update->id)->first();

        return response()->json(response_success(
            "success update data",
            $user,
        ), 200);
    }

    public function userEditView(Request $request, $id = 0)
    {
        $user = User::where('id', $id)->first();
        if (!$user) {
            return redirect()
                ->route('user-view')
                ->withErrors('data user tidak di temukan');
        }

        $configuration = Configuration::first();

        return view('pages/user/user-edit', [
            'user' => $user,
            'configuration' => $configuration,
        ]);
    }

    public function userEdit(Request $request, $id = 0)
    {
        $rule = [
            'name' => 'required|min:1|max:100',
            'email' => 'required|email',
            'username' => 'required|min:1|max:100',
            'password' => 'nullable|min:1|max:200',
            'role' => 'required|in:ADMIN,USER,WO',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // set data
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'role' => $request->role,
        ];

        if (isset($request->password) && !empty($request->password)) {
            $data['password'] = Hash::make($request->password);
        }

        // update data
        $update = User::where('id', $id)->first()->update($data);
        if (!$update) {
            return response()->json(response_error(
                "error update data"
            ), 200);
        }

        $user = User::where('id', $id)->first();

        return response()->json(response_success(
            "success update data",
            $user,
        ), 200);
    }

    public function userRemove(Request $request)
    {
        $rule = [
            'id' => 'required|exists:users,id',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // delete data
        $delete = User::where('id', $request->id)->first()->delete();
        if (!$delete) {
            return response()->json(response_error(
                "error remove data"
            ), 200);
        }

        return response()->json(response_success(
            "success remove data",
            [],
        ), 200);
    }

    public function userGet(Request $request)
    {
        // set pagination limit
        $per_page = (!empty($request->get('size'))) ? $request->get('size') : 10;

        // set current page
        $page = (!empty($request->get('page'))) ? $request->get('page') : 1;

        // set offset
        $offset = ($page - 1) * $per_page;

        // set order by
        $order_by = (!empty($request->get('sorters'))) ? $request->get('sorters') : [
            [
                'field' => 'created_at',
                'dir' => 'asc'
            ],
        ];

        // set filters
        $filters = (!empty($request->get('filters'))) ? $request->get('filters') : [];

        // user query
        $user_query = User::query();

        // set sorters
        foreach ($order_by as $orderby) {
            $user_query->orderBy($orderby['field'], $orderby['dir']);
        }

        // set filters
        foreach ($filters as $filter) {
            $user_query->where($filter['field'], $filter['type'], "%" . $filter['value'] . "%");
        }

        // set result
        $total = $user_query->count();
        $users = $user_query->skip($offset)->take($per_page)
            ->get()
            ->toArray();

        // set pagination
        $lastpage = 1;
        if ($total > 0) {
            $pagination = new LengthAwarePaginator($users, $total, $per_page, $page, ['path' => $request->url(), 'query' => $request->query()]);
            $lastpage = $pagination->lastPage();
        }

        return response()->json([
            'data' => $users,
            'last_page' => $lastpage,
        ], 200);
    }
}
